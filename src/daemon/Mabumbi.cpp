/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#include "Mabumbi.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>


Mabumbi::Mabumbi()
        :destinationConnect(false)
{
    readConfig();

    connect( &backupProcess, SIGNAL(error(QProcess::ProcessError)), this, SIGNAL(error()) );
    connect( &backupProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(backupFinished()) );
    connect( &backupProcess, SIGNAL(started()), this, SLOT(backupStarted()) );

    connect( &stateCheckTimer, SIGNAL(timeout()), this, SLOT(checkDestination()) );
    stateCheckTimer.start( 10*1000 );

    connect( &backupTimer, SIGNAL(timeout()), this, SLOT(startBackup()) );
    startBackupIntervall();
}

Mabumbi::~Mabumbi()
{
}

void Mabumbi::startBackup()
{
    if ( isBackupRunning() )
        return;
    if ( !destinationAvailable() )
        return;

    QString command = config.getExecutable() +" -f " + config.getStoreBackupConfigFile();
//    qDebug() << command;
    backupProcess.start( command );
}

void Mabumbi::checkState()
{
    if ( isBackupRunning() )
    {
        emit startingBackup();
    }
    else
    {
        emit idleEntered();

        destinationConnect = destinationAvailable();
        if ( !destinationConnect )
            emit destinationDisconnected();
    }
}

void Mabumbi::checkDestination()
{
    if ( !destinationAvailable() )
    {
        if ( destinationConnect )
        {
            destinationConnect = false;
            emit destinationDisconnected();
        }
    }
    else
    {
        if ( !destinationConnect )
        {
            destinationConnect = true;
            emit destinationConnected();
            if ( config.startBackupOnTargetInstantly() )
                startBackup();
        }
    }
}

QString Mabumbi::getBackupDir()
{
    return config.getDestination();
}

QString Mabumbi::getStoreBackupConfigFile()
{
    return config.getStoreBackupConfigFile();
}

QString Mabumbi::getUUID()
{
    return config.getDeviceUUID();
}

bool Mabumbi::destinationAvailable()
{
    QDir ddir( config.getDestination() );
    return ddir.exists();
}

bool Mabumbi::isBackupRunning()
{
    return !(backupProcess.state()==QProcess::NotRunning);
}

void Mabumbi::readConfig()
{
    QString configFile = "/etc/mabumbid.conf";
    QStringList args = QCoreApplication::arguments();
    if ( args.size() == 3 )
    {
        if ( args[1]=="-f" )
            configFile = args[2];
    }
    config.readConfig( configFile );
//    config.dumpConfig();
}

void Mabumbi::backupStarted()
{
    emit startingBackup();
    backupTimer.stop();
}

void Mabumbi::backupFinished()
{
    emit idleEntered();
    startBackupIntervall();
    if ( config.unmountTargetAfterBackup() )
        unmountTarget();
}

void Mabumbi::reloadConfiguration()
{
    readConfig();
    startBackupIntervall();
}

void Mabumbi::startBackupIntervall()
{
    if ( config.hourlyBackupIntervall() > 0 )
        backupTimer.start( config.hourlyBackupIntervall()*60*60*1000 );
}

void Mabumbi::unmountTarget()
{
    QString command = "umount /dev/disk/by-uuid/" + config.getDeviceUUID();
    QProcess::execute( command );
}
