/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#include "Configuration.h"

#include <QDebug>
#include <QFile>
#include <QTextStream>

Configuration::Configuration()
        :executable("/usr/bin/storeBackup"),
        storeBackupConfigFile("/etc/storeBackup.conf"),
        backupDir("/media/Disk/Backup"),
        intervall(1),
        instantBackupOnTarget(false),
        unmountAfterBackup(false),
        uuid("")
{
}

Configuration::~Configuration()
{
}

const QString& Configuration::getExecutable() const
{
    return executable;
}

const QString& Configuration::getStoreBackupConfigFile() const
{
    return storeBackupConfigFile;
}

const QString& Configuration::getDestination() const
{
    return backupDir;
}

int Configuration::hourlyBackupIntervall() const
{
    return intervall;
}

bool Configuration::startBackupOnTargetInstantly() const
{
    return instantBackupOnTarget;
}

bool Configuration::unmountTargetAfterBackup() const
{
    return unmountAfterBackup;
}

const QString& Configuration::getDeviceUUID() const
{
    return uuid;
}

void Configuration::readConfig( const QString& configFile )
{
    readMabumbiConfig( configFile );
    readStoreBackupConfig();
}

void Configuration::readMabumbiConfig( const QString& configFile )
{
    QFile cfile( configFile );
    if ( !cfile.exists() )
    {
        qDebug() << "Can't find configuration file: " << configFile;
        return;
    }
    bool ok;
    ok = cfile.open( QIODevice::ReadOnly );
    if ( !ok )
    {
        qDebug() << "Can't open file: " << configFile;
        return;
    }

    QTextStream stream( &cfile );
    QString line;
    while ( !stream.atEnd() )
    {
        line = stream.readLine().trimmed();
        if ( !line.startsWith( "#" ) )
        {
            if ( line.startsWith( "storeBackupExec=" ) )
                executable = line.right( line.length()-16 );
            if ( line.startsWith( "storeBackupConfigFile=" ) )
                storeBackupConfigFile = line.right( line.length()-22 );
            if ( line.startsWith( "intervall=" ) )
            {
                intervall = line.right( line.length()-10 ).toInt( &ok );
                if ( !ok )
                    intervall = 1;
            }
            if ( line.startsWith( "startBackupAsTargetIsAvailable=" ) )
                instantBackupOnTarget = line.right( line.length()-31 ) == "yes";
            if ( line.startsWith( "unmountAfterBackup=" ) )
                unmountAfterBackup = line.right( line.length()-19 ) == "yes";
            if ( line.startsWith( "backupDeviceUUID=" ) )
                uuid = line.right( line.length()-17 );
        }
    }
    cfile.close();
    //dumpConfig();
}

void Configuration::readStoreBackupConfig()
{
    backupDir = QString();
    QFile sbFile( storeBackupConfigFile );
    if ( !sbFile.exists() )
    {
        qDebug() << "Can't find storebackup configuration file: " << storeBackupConfigFile;
        return;
    }
    bool ok;
    ok = sbFile.open( QIODevice::ReadOnly );
    if ( !ok )
    {
        qDebug() << "Can't open storebackup configuration file: " << storeBackupConfigFile;
        return;
    }

    QTextStream stream( &sbFile );
    QString line;
    while ( !stream.atEnd() )
    {
        line = stream.readLine().trimmed();
        if ( line.startsWith( "targetDir=" ) )
            backupDir = line.right( line.length()-10 );
        if ( line.startsWith( "backupDir=" ) )
            backupDir = line.right( line.length()-10 );
    }
    sbFile.close();
    backupDir = backupDir.trimmed();

    if ( backupDir.isEmpty() )
        qDebug() << "No backup directory found in storebackup configuration";
}

void Configuration::dumpConfig()
{
    qDebug() << "Executable:" << executable;
    qDebug() << "ConfigFile:" << storeBackupConfigFile;
    qDebug() << "Intervall:" << intervall;
    qDebug() << "BackupDir:" << backupDir;
    qDebug() << "instantBackupOnTarget:" << instantBackupOnTarget;
    qDebug() << "unmountAfterBackup:" << unmountAfterBackup;
    qDebug() << "UUID:" << uuid;
}
