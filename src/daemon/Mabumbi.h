/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#ifndef Mabumbi_Mabumbi_H
#define Mabumbi_Mabumbi_H

#include "Configuration.h"

#include <QtCore/QObject>
#include <QProcess>
#include <QTimer>

class Mabumbi : public QObject
{
Q_OBJECT
public:
    Mabumbi();
    virtual ~Mabumbi();

    bool destinationAvailable();
    bool isBackupRunning();
    void startBackupIntervall();

public Q_SLOTS:
    void startBackup();
    void checkState();
    QString getBackupDir();
    QString getStoreBackupConfigFile();
    QString getUUID();
    void reloadConfiguration();

    void checkDestination();
    void backupStarted();
    void backupFinished();

Q_SIGNALS:
    void startingBackup();
    void idleEntered();
    void destinationConnected();
    void destinationDisconnected();
    void error();

private:
    void readConfig();
    void unmountTarget();

    Configuration config;
    QProcess backupProcess;
    QTimer backupTimer;
    QTimer stateCheckTimer;
    bool destinationConnect;
    bool backupRunning;
};

#endif // Mabumbi_Mabumbi_H
