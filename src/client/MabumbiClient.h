/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#ifndef Mabumbi_MabumbiClient_H
#define Mabumbi_MabumbiClient_H

#include <QMainWindow>

#include <QString>

class OrgMabumbiBackupMabumbiInterface;
class BackupStatisticsWidget;
class MabumbiSysTray;
class QAction;
class QCloseEvent;
class QTimer;

class MabumbiClient : public QMainWindow
{
Q_OBJECT
public:
    MabumbiClient();
    ~MabumbiClient();

public Q_SLOTS:
    void startBackup();
    void openBackup();
    void checkDBusConnection();
    void startCheck();
    void quit();
    void fillStatisticsWidget();
    void configureStoreBackup();
    void configureDaemon();
    void editDevices();

    void backupEnteredIdle();
    void backupEnteredRunning();
    void backupDestinationNotAvailable();
    void backupError();

protected:
    void closeEvent( QCloseEvent* event );

private:
    void setupActions();
    void initDBusConnection();
    void runAsRoot( const QString& command );

    MabumbiSysTray* tray;
    OrgMabumbiBackupMabumbiInterface* daemon;
    QTimer* timer;
    bool connected;
    QAction* actionStart;
    QAction* actionOpen;
    BackupStatisticsWidget* statisticsWidget;
};

#endif // Mabumbi_Mabumbi_H
