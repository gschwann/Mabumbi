/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#ifndef Mabumbi_MabumbiSysTray_H
#define Mabumbi_MabumbiSysTray_H

#include <QSystemTrayIcon>
#include <QIcon>
#include <QTimer>
#include <QVector>

class KAction;
class KActionCollection;

class MabumbiSysTray : public QSystemTrayIcon
{
    Q_OBJECT
public:
    MabumbiSysTray( QWidget* parent = nullptr );
    virtual ~MabumbiSysTray();

    void buildContextMenu();
    QIcon runningIcon();
    QIcon idleIcon();
    QIcon disconnectedIcon();
    QIcon errorIcon();

public Q_SLOTS:
    void backupRunning();
    void backupIdle();
    void destinationConnected();
    void destinationDisconnected();
    void backupError();
    void animate();

Q_SIGNALS:
    void requestStartBackup();
    void requestOpenBackup();

private:
    void loadRunningIcons();

    QVector<QIcon> runIcons;
    int animationIdx;
    QTimer animationTimer;
};

#endif
