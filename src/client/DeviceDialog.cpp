/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#include "DeviceDialog.h"
#include "BackupDevice.h"

#include "ui_DeviceDialogUI.h"

//#include <KDE/KConfig>
//#include <KDE/KDebug>
//#include <KDE/KGlobal>

#include <QStringList>

DeviceDialog::DeviceDialog( QWidget* parent )
        : QDialog(parent)
        , ui(new Ui::DeviceDialogUI)
{
    ui->setupUi( this );
//    setMainWidget( frame );

//    KSharedConfigPtr config = KGlobal::config();
//    KConfigGroup generalGroup( config, "General" );
//    QString currentUUID = generalGroup.readEntry( "BackupUUID", QString("9fd7276a-a337-4642-9c26-6b4db475b112") );
//    bool automount = generalGroup.readEntry( "AutoMount", true );
//    automountCheckBox->setChecked( automount );

//    QStringList uuids = BackupDevice::externalDriveUIDs();
}

DeviceDialog::~DeviceDialog()
{
    //KSharedConfigPtr config = KGlobal::config();
    //KConfigGroup generalGroup( config, "General" );
    //generalGroup.writeEntry( "BackupUUID", currentUUID );
    //generalGroup.writeEntry( "AutoMount", automountCheckBox->checked() );
    //generalGroup.config()->sync();
}

void DeviceDialog::initBackup()
{
}
