/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#include "MabumbiClient.h"

#include <QApplication>

//#include <KDE/KAboutData>
//#include <KDE/KCmdLineArgs>
//#include <KDE/KUniqueApplication>

int main(int argc, char *argv[])
{
//    KAboutData about( "Mabumbi", "Mabumbi", ki18n("Mabumbi"), "0.1.3",
//                    ki18n("A GUI for storeBackup"),
//                    KAboutData::License_GPL_V2,
//                    ki18n("Copyright (c) 2009, Guenter Schwann"),
//                    ki18n("This application is inspired by Apples Timemachine.\nThe actual backup is done by storeBackup"),
//                    "http://www.schwann.at/mabumbi",
//                    "harry.w@gmx.at");
//    about.addAuthor( ki18n("Guenter Schwann"), ki18n("developer"), "harry.w@gmx.at", 0 );
//    KCmdLineArgs::init(argc, argv, &about);
    QApplication app(argc, argv);
    MabumbiClient mc;
//    mc.show();
    return app.exec();
}
