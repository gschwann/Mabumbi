/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#include "BackupDevice.h"

#include <QDebug>

//#include <KDiskFreeSpaceInfo>
//#include <KDE/Solid/Device>
//#include <KDE/Solid/DeviceNotifier>
//#include <KDE/Solid/StorageDrive>

BackupDevice::BackupDevice( const QString& uuid )
//        :volume(0),
//        vaccess(0)
{
    if ( !uuid.isEmpty() )
        init( uuid );
}

BackupDevice::~BackupDevice()
{
}

void BackupDevice::init( const QString& uuid )
{
//    QList<Solid::Device> devs = Solid::Device::listFromType( Solid::DeviceInterface::StorageVolume );
//    foreach ( Solid::Device device, devs )
//    {
//        Solid::StorageVolume* vol = device.as<Solid::StorageVolume>();
//        Solid::StorageAccess* acc = device.as<Solid::StorageAccess>();
//        if ( vol!=0 && acc!=0 )
//        {
//            if ( vol->uuid() == uuid )
//            {
//                volume = vol;
//                vaccess = acc;
//            }
//        }
//    }
}

bool BackupDevice::mounted() const
{
//    if ( vaccess==0 )
//        return false;
//    return vaccess->isAccessible();
    return false;
}

void BackupDevice::mount()
{
//    if ( vaccess==0 )
//        return;
//    vaccess->setup();
}

qulonglong BackupDevice::size() const
{
//    if ( volume==0 )
//        return 0;
//    return volume->size();
    return 0;
}

qulonglong BackupDevice::sizeInGB() const
{
    return size() / (1024*1024*1024);
}

qulonglong BackupDevice::used() const
{
    if ( !mounted() )
        return 0;

//    KDiskFreeSpaceInfo info = KDiskFreeSpaceInfo::freeSpaceInfo( vaccess->filePath() );
//    return info.used();
    return 0;
}

qulonglong BackupDevice::usedInGB() const
{
    return used() / (1024*1024*1024);
}

qulonglong BackupDevice::freeSpace() const
{
    if ( !mounted() )
        return 0;
    return (size() - used());
}

qulonglong BackupDevice::freeSpaceInGB() const
{
    return freeSpace() / (1024*1024*1024);
}

QString BackupDevice::label() const
{
//    if ( volume==0 )
//        return QString( "" );
//    return volume->label();
    return {};
}

QStringList BackupDevice::externalDriveUIDs()
{
    QStringList uids;
//    QList<Solid::Device> driveDevices = Solid::Device::listFromType( Solid::DeviceInterface::StorageDrive, QString() );
//    foreach ( Solid::Device driveDevice, driveDevices )
//    {
//        Solid::StorageDrive* drive = driveDevice.as<Solid::StorageDrive>();
//        if ( drive->bus()==Solid::StorageDrive::Usb || drive->bus()==Solid::StorageDrive::Ieee1394 )
//        {
//            uids.append( driveDevice.udi() );
//            //kDebug() << driveDevice.udi();
//        }
//    }
    return uids;
}

void BackupDevice::printCandiates()
{
//    QStringList driveUids = BackupDevice::externalDriveUIDs();
//    if ( driveUids.empty() )
//        return;

//    QList<Solid::Device> devices;
//    foreach ( const QString puid, driveUids )
//    {
//        QList<Solid::Device> devs = Solid::Device::listFromType( Solid::DeviceInterface::StorageVolume, puid );
//        devices.append( devs );
//    }

//    foreach ( Solid::Device device, devices )
//    {
//        Solid::StorageVolume* vol = device.as<Solid::StorageVolume>();
//        Solid::StorageAccess* vaccess = (Solid::StorageAccess*)device.asDeviceInterface( Solid::DeviceInterface::StorageAccess );
//        if ( vaccess!=0 )
//        {
//            //if ( !vaccess->isAccessible() )
//            //    vaccess->setup();
//            kDebug() << vol->label() << " | " << vol->fsType() << " | " << vaccess->filePath() << " | " << vol->size()/(1024*1024*1024) << "GB";
//            if ( vaccess->isAccessible() )
//            {
//                //vaccess->teardown();
//                KDiskFreeSpaceInfo info = KDiskFreeSpaceInfo::freeSpaceInfo(vaccess->filePath());
//                info.used();
//                kDebug() << ((info.used()*100) / info.size() ) << "% used (" << info.used()/(1024*1024*1024)<< "GB) | " << (info.size()-info.used())/(1024*1024*1024) << "GB free";
//            }
//        }
//    }
}
