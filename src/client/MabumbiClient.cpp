/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#include "MabumbiClient.h"
#include "BackupDevice.h"
#include "BackupStatisticsWidget.h"
#include "DeviceDialog.h"
#include "MabumbiInterface.h"
#include "MabumbiSysTray.h"

//#include <kpassworddialog.h>
//#include <KDE/Solid/DeviceNotifier>
//#include <KDE/KDEsuClient>
//#include <KDE/SuProcess>

#include <QApplication>
#include <QCloseEvent>
#include <QDesktopServices>
#include <QIcon>
#include <QMenu>
#include <QMessageBox>
#include <QTimer>
#include <QUrl>

MabumbiClient::MabumbiClient()
        : QMainWindow(0)
        , connected(false)
        , daemon(nullptr)
{
    statisticsWidget = new BackupStatisticsWidget( this );
    this->setCentralWidget( statisticsWidget );
    statisticsWidget->setEnabled( false ); /// @TODO temporary

//    setupActions();
//    setupGUI();

    tray = new MabumbiSysTray(this);
    tray->show();

    initDBusConnection();

    timer = new QTimer( this );
    connect( timer, SIGNAL(timeout()), this, SLOT(checkDBusConnection()) );
    timer->start( 5000 );
    timer->singleShot( 200, this, SLOT(startCheck()) );

    fillStatisticsWidget();
//    Solid::DeviceNotifier* notify = Solid::DeviceNotifier::instance();
//    connect( notify, SIGNAL(deviceAdded(const QString&)), this, SLOT(fillStatisticsWidget()) );
}

MabumbiClient::~MabumbiClient()
{
}

void MabumbiClient::closeEvent( QCloseEvent* event )
{
    event->ignore();
    quit();
}

void MabumbiClient::setupActions()
{
//    KAction* mdc = new KAction( this );
//    mdc->setText("Configure daemon..." );
//    connect( mdc, SIGNAL(triggered(bool)), this, SLOT(configureDaemon()) );
//    actionCollection()->addAction( "Config_Daemon", mdc );

//    KAction* sbc = new KAction( this );
//    sbc->setText( "Configure storeBackup..." );
//    connect( sbc, SIGNAL(triggered(bool)), this, SLOT(configureStoreBackup()) );
//    actionCollection()->addAction( "Config_storeBackup", sbc );

//    KAction* da = new KAction( this );
//    da->setText( "Devices..." );
//    connect( da, SIGNAL(triggered(bool)), this, SLOT(editDevices()) );
//    actionCollection()->addAction( "devices", da );
//    da->setEnabled( false ); /// @TODO temporary

//    actionStart = new KAction( this );
//    actionStart->setText( "Start Backup" );
//    connect( actionStart, SIGNAL(triggered(bool)), this, SLOT(startBackup()) );
//    actionCollection()->addAction( "Start_Backup", actionStart );

//    actionOpen = new KAction( this );
//    actionOpen->setText( "Open Backup" );
//    connect( actionOpen, SIGNAL(triggered(bool)), this, SLOT(openBackup()) );
//    actionCollection()->addAction( "Open_Backup", actionOpen );

//    KStandardAction::quit( kapp, SLOT(quit()), actionCollection() );
}

void MabumbiClient::initDBusConnection()
{
    daemon = new OrgMabumbiBackupMabumbiInterface("org.mabumbi.backup", "/mabumbi",
        QDBusConnection::systemBus(), this);
//        QDBusConnection::sessionBus(), this ); // testing

    connect(daemon, &OrgMabumbiBackupMabumbiInterface::startingBackup, this, &MabumbiClient::backupEnteredRunning);
    connect(daemon, &OrgMabumbiBackupMabumbiInterface::idleEntered, this, &MabumbiClient::backupEnteredIdle);
    connect(daemon, &OrgMabumbiBackupMabumbiInterface::error, this, &MabumbiClient::backupError);
    connect(daemon, &OrgMabumbiBackupMabumbiInterface::destinationConnected, this, &MabumbiClient::backupEnteredIdle);
    connect(daemon, &OrgMabumbiBackupMabumbiInterface::destinationDisconnected, this, &MabumbiClient::backupDestinationNotAvailable);
}

void MabumbiClient::startBackup()
{
    if ( daemon == nullptr ) {
        return;
    }

    daemon->startBackup();
}

void MabumbiClient::openBackup()
{
    if ( daemon == nullptr ) {
        return;
    }

    QDBusPendingReply<QString> reply = daemon->getBackupDir();
    reply.waitForFinished();
    if( reply.isError() )
    {
        tray->showMessage( reply.error().name(), reply.error().message() );
    }
    else
    {
        QString dir = reply.value();
        QUrl url( dir );
        QDesktopServices::openUrl( url );
    }
}

void MabumbiClient::quit()
{
    QApplication::exit();
}

void MabumbiClient::startCheck()
{
    if ( daemon->isValid() )
    {
        connected = true;
        daemon->checkState();
    }
    else
    {
        connected = false;
        tray->backupError();
//        actionStart->setDisabled( true );
//        actionOpen->setDisabled( true );
    }
}

void MabumbiClient::checkDBusConnection()
{
    if ( daemon == nullptr )
    {
        initDBusConnection();
        return;
    }

    if ( daemon->isValid() )
    {
        if ( !connected )
        {
            connected = true;
            daemon->checkState();
        }
    }
    else
    {
        if ( connected )
        {
            connected = false;
            tray->backupError();
        }
        delete( daemon );
        daemon = nullptr;
        initDBusConnection();
    }
}

void MabumbiClient::fillStatisticsWidget()
{
//    KSharedConfigPtr config = KGlobal::config();
//    KConfigGroup generalGroup( config, "General" );
//    //QString uuid = generalGroup.readEntry( "BackupUUID", QString("9fd7276a-a337-4642-9c26-6b4db475b112") );
//    QString uuid = daemon->getUUID();
//    BackupDevice bd( uuid );
//    statisticsWidget->setName( bd.label() );
//    statisticsWidget->setCapacity( bd.sizeInGB() );
//    statisticsWidget->setFreeSpace( bd.freeSpaceInGB() );
}

void MabumbiClient::configureStoreBackup()
{
    QString configFile;
//    QDBusPendingReply<QString> reply = daemon->getStoreBackupConfigFile();
//    reply.waitForFinished();
//    if( reply.isError() )
//        tray->showMessage( reply.error().name(), reply.error().message() );
//    else
//        configFile = reply.value();

//    QString command = "kwrite " + configFile;
//    runAsRoot( command );
}

void MabumbiClient::configureDaemon()
{
//    QString configFile = "/etc/mabumbid.conf";
//    QString command = "kwrite " + configFile;
//    runAsRoot( command );
//    if ( daemon!=0 )
//        daemon->reloadConfiguration();
}

void MabumbiClient::runAsRoot( const QString& command )
{
//    qDebug() << command;

//    KDESu::SuProcess sup( "root", command.toLocal8Bit() );

//    KPasswordDialog dlg( this );
//    dlg.setCaption( i18n("Authorization Required") );
//    if ( sup.useUsersOwnPassword() )
//        dlg.setPrompt( i18n("The requested action requires administrator privileges.\n"
//                       "If you have these privileges then please enter your password.") );
//    else
//        dlg.setPrompt( i18n("The requested action requires administrator privileges.\n"
//                       "Please enter the system administrator's password.") );
//    dlg.setPixmap( DesktopIcon("dialog-password") );
//    if ( dlg.exec() )
//    {
//        QString passwd = dlg.password();
//        sup.exec( passwd.toLocal8Bit() );
//    }
}

void MabumbiClient::editDevices()
{
//    DeviceDialog dd( this );
//    dd.exec();
}

void MabumbiClient::backupEnteredIdle()
{
    tray->backupIdle();
//    actionStart->setDisabled( false );
//    actionOpen->setDisabled( false );
}

void MabumbiClient::backupEnteredRunning()
{
    tray->backupRunning();
//    actionStart->setDisabled( true );
//    actionOpen->setDisabled( false );
}

void MabumbiClient::backupDestinationNotAvailable()
{
    tray->destinationDisconnected();
//    actionStart->setDisabled( true );
//    actionOpen->setDisabled( true );
}

void MabumbiClient::backupError()
{
    tray->backupError();
}
