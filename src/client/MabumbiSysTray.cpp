/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#include "MabumbiSysTray.h"

#include <QAction>

#include <QDebug>
#include <QMenu>

MabumbiSysTray::MabumbiSysTray( QWidget* parent )
        : QSystemTrayIcon(parent),
        animationIdx(0)
{
    buildContextMenu();

    animationTimer.setInterval( 600 );
    animationTimer.setSingleShot( false );
    connect( &animationTimer, SIGNAL(timeout()), this, SLOT(animate()) );

    setIcon( idleIcon() );
}

MabumbiSysTray::~MabumbiSysTray()
{
}

QIcon MabumbiSysTray::runningIcon()
{
    if ( runIcons.isEmpty() )
    {
        loadRunningIcons();
        if ( runIcons.isEmpty() )
            return QIcon();
    }
    return runIcons[0];
}

void MabumbiSysTray::loadRunningIcons()
{
    runIcons.append(QIcon(":/icons/running01.png"));
    runIcons.append(QIcon(":/icons/running02.png"));
    runIcons.append(QIcon(":/icons/running03.png"));
}

QIcon MabumbiSysTray::idleIcon()
{
    return QIcon(":/icons/isidle.png");
}

QIcon MabumbiSysTray::disconnectedIcon()
{
    return QIcon(":/icons/disconnect.png");
}

QIcon MabumbiSysTray::errorIcon()
{
    return QIcon(":/icons/error.png");
}

void MabumbiSysTray::backupRunning()
{
    setIcon( runningIcon() );
    animationTimer.start();
    setToolTip( tr("Backup is running") );
}

void MabumbiSysTray::backupIdle()
{
    animationTimer.stop();
    setIcon( idleIcon() );
    setToolTip( tr("Backup is idle") );
}

void MabumbiSysTray::destinationConnected()
{
    backupIdle();
}

void MabumbiSysTray::destinationDisconnected()
{
    animationTimer.stop();
    setIcon( disconnectedIcon() );
    setToolTip( tr("Backup destination is not available") );
}

void MabumbiSysTray::backupError()
{
    animationTimer.stop();
    setIcon( errorIcon() );
    setToolTip( tr("Backup error") );
}

void MabumbiSysTray::buildContextMenu()
{
    QMenu* contextMenu = new QMenu();
    QAction* startAction = contextMenu->addAction(tr("Start Backup"));
    connect(startAction, &QAction::triggered, this, [&](){
        qDebug() << "Start backup triggered";
        Q_EMIT requestStartBackup();
    });
    QAction* openAction = contextMenu->addAction(tr("Open Backup"));
    connect(openAction, &QAction::triggered, this, [&](){
        qDebug() << "Open backup triggered";
        Q_EMIT requestOpenBackup();
    });

    setContextMenu(contextMenu);
}

void MabumbiSysTray::animate()
{
    if ( runIcons.isEmpty() )
    {
        animationTimer.stop();
        return;
    }
    ++animationIdx;
    animationIdx = animationIdx % runIcons.size();
    setIcon( runIcons[animationIdx] );
}
