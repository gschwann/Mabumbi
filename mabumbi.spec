#
#

Name:           mabumbi
BuildRequires:  libkde4-devel
Requires:       storeBackup
License:        GPL v2 or later
Group:          Productivity/Other
Summary:        Backup program
Url:            http://www.schwann.at/mabumbi
Version:        0.2.0
Release:        1.0
Source0:        %name-%version.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
Backup program, that aims to be very easy to use


Authors:
--------
    Guenter Schwann <harry.w@gmx.at>

%prep
%setup -q -n Mabumbi

%build
  %cmake_kde4 -d build
  %make_jobs

%install
  cd build
  %makeinstall
  %create_exclude_filelist
  %kde_post_install

%post -p /sbin/ldconfig
/etc/init.d/dbus reload

%postun -p /sbin/ldconfig

%clean
  rm -rf $RPM_BUILD_ROOT
  rm -rf filelists

%files
%config /etc/mabumbid.conf
%config /etc/dbus-1/system.d/mabumbi.conf
%config /usr/lib/systemd/system/mabumbi.service
/usr/bin/mabumbiclient
/usr/sbin/mabumbid
/usr/share/applications/kde4/mabumbi.desktop
/usr/share/kde4/apps/Mabumbi/Mabumbiui.rc
/usr/share/icons/oxygen/128x128/apps/mabumbi.png
/usr/share/icons/oxygen/16x16/apps/mabumbi.png
/usr/share/icons/oxygen/22x22/apps/mabumbi.png
/usr/share/icons/oxygen/32x32/actions/mabumbi-disconnected.png
/usr/share/icons/oxygen/32x32/actions/mabumbi-error.png
/usr/share/icons/oxygen/32x32/actions/mabumbi-isidle.png
/usr/share/icons/oxygen/32x32/actions/mabumbi-running01.png
/usr/share/icons/oxygen/32x32/actions/mabumbi-running02.png
/usr/share/icons/oxygen/32x32/actions/mabumbi-running03.png
/usr/share/icons/oxygen/32x32/apps/mabumbi.png
/usr/share/icons/oxygen/48x48/apps/mabumbi.png
/usr/share/icons/oxygen/64x64/apps/mabumbi.png

%changelog
* Fri Dec 20 2013 Guenter Schwann <harry.w@gmx.at>
- switch to systemd

* Fri Apr 10 2009 Guenter Schwann <harry.w@gmx.at>
- first version
