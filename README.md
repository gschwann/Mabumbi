# Mabumbi

This is a desktop backup application Linux. It is heavily inspired by Apple's Time Machine. Currently Mabumbi focuses to make backups on USB harddrives as comfortable as possible.
My goal is to make it even usable by my father (I know it's a high bar ;-)
